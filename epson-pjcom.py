import subprocess
import sys

# Function to send an SNMP SET command to control the projector
def send_snmp_command(ip_address, oid, value):
    command = f"snmpset -v2c -c private {ip_address} {oid} i {value}"
    subprocess.run(command, shell=True)

# Function to control the projector power state
def control_projector_power(ip_address, power_state):
    oid = "1.3.6.1.4.1.1248.4.1.1.2.1.0"  # OID for power control
    send_snmp_command(ip_address, oid, power_state)

# Usage example
if len(sys.argv) < 3:
    print("Please provide the IP address of the Epson projector and power state (0 or 1) as arguments.")
    sys.exit(1)

projector_ip = sys.argv[1]
power_state = int(sys.argv[2])

if power_state not in [0, 1]:
    print("Invalid power state. Please provide 0 for off or 1 for on.")
    sys.exit(1)

control_projector_power(projector_ip, power_state)
